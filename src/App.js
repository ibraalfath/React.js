import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './bootstrap.min.css';
import Inputan from './inputan/Inputan';
import Footer from './footer/Footer';

class App extends Component {
  render() {
    return (
      <div>
        <h1 className="jumbotron text-center">Pendaftaran Santri Baru Pondok IT</h1>
        <Inputan/>
        <br/>
        <Footer/>
      </div>
    );
  }
}

export default App;
